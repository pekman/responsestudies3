import os


class Config:
    def __init__(self, project_name):
        self.project_name = project_name
        self.path = f"{os.path.abspath(os.getcwd())}/projects/{project_name}/"
        self.data_path = self.path + "data/"
        self.output_path = self.path + "output/"
        self.listOfRootFilePaths = [self.data_path + "merged.root"]

        self.slices = [[-0.8, 0.8]]
        self.slicingAxis = "z"
        self.projectionAxis = "y"
        self.projectionRebinWidth = 4
        self.responseAxis = "x"

        self.nSigmaForFit = 1.3
        self.fitOptString = "RESQ"

        self.plotDict = {
            "pt": {
                "xLimits": (50, 4000),
                "yLimits": (0.990, 1.01),
                "xScale": "linear",
                "xAxisLabel": "Truth pT [GeV]",
                "yAxisLabel": "Matched reco/truth pT",
                "legendTitle": ["Eta=[", "], Matched leading and subleading jets"],
            },
        }
