import ROOT
from modules.JES_BalanceFitter import JES_BalanceFitter
import numpy as np
import pandas as pd


def process(config):
    # Loop over the file paths
    for rootFilePath in config.listOfRootFilePaths:
        # Open root file containing 3D histograms
        inFile = ROOT.TFile.Open(rootFilePath)
        # Loop over the slices
        for currentSlice in config.slices:
            listOfKeys = (
                inFile.GetListOfKeys()
            )  # Get the 3D histogram names from the root file

            dictionaryList = (
                []
            )  # Initialize an empty list to house the dictionaries, one per slice

            # Loop over the 3D Histogram names
            for key in listOfKeys:
                TH3Name = key.GetName()  # Get the actual name as a string

                if (
                    TH3Name[0:9] != "scaled_h_"
                ):  # Skip the #D histograms which are not scaled
                    continue
                elif (
                    inFile.Get(TH3Name).GetEntries() == 0.0
                ):  # Print a warning if a 3D histogram is empty
                    print("WARNING:", TH3Name, " is empty!")
                    continue
                else:  # If everything is good, append an empty pandas series to the dictionary list
                    dictionaryList.append(
                        pd.Series(
                            {
                                "x": [],
                                "y": [],
                                "xError": [],
                                "yError": [],
                                "sigma": [],
                                "sigmaError": [],
                                "sigmaOverY": [],
                                "sigmaOverYError": [],
                                "fitAmplitude": [],
                                "fitMin": [],
                                "fitMax": [],
                                "TH1BinEdges": [],
                                "TH1BinEntries": [],
                                "TH1BinErrors": [],
                                "gaussianParameters": [],
                            },
                            name=TH3Name,
                        )
                    )
            # Define a path and name for the dataframe based on the slice range
            dfPath = (
                config.output_path
                + "pickles/"
                + config.project_name
                + "_"
                + config.slicingAxis
                + "["
                + str(currentSlice[0])
                + ","
                + str(currentSlice[1])
                + "]"
            )
            df = pd.DataFrame(
                dictionaryList
            )  # Create the dataframe form the dictionary list

            # Within the current slice, loop over the 3D histogram names in the empty dataframe
            for TH3Name in reversed(df.index):
                if "Log" in TH3Name:
                    continue
                print(TH3Name)
                inTH3 = inFile.Get(
                    TH3Name
                )  # Retreive the current 3D histogram form the root file
                h3D = inTH3.Clone()  # Clone it ot keep it intact

                # Set fitting options for the JES_BalanceFitter tool
                JESBfitter = JES_BalanceFitter(config.nSigmaForFit)
                JESBfitter.SetGaus()
                JESBfitter.SetFitOpt(config.fitOptString)

                # Set the 3D histogram range to correspond to the desired slice range
                if config.slicingAxis == "y":
                    h3D.GetYaxis().SetRangeUser(currentSlice[0], currentSlice[1])
                elif config.slicingAxis == "z":
                    h3D.GetZaxis().SetRangeUser(currentSlice[0], currentSlice[1])

                # Project the 3D histogram with the sliced axis range into a 2D histogram
                h2D = h3D.Project3D(f"{config.responseAxis}{config.projectionAxis}")

                # Rebin the 2D histogram x-axis according to the desired rebinningFactor
                h2D.RebinX(config.projectionRebinWidth)

                # Loop over the bins in the 2D histogram x-axis
                for currentRebinnedBin in range(
                    1, h2D.GetNbinsX() + 1
                ):  # Histograms start at bin 1, plus one to include last bin
                    # Name of the 1D projection
                    projName = (
                        "slice"
                        + str(currentSlice[0])
                        + "to"
                        + str(currentSlice[1])
                        + "_projectionBin"
                        + str(h2D.GetXaxis().GetBinLowEdge(currentRebinnedBin))
                        + "to"
                        + str(h2D.GetXaxis().GetBinUpEdge(currentRebinnedBin))
                    )

                    # Project the current 2D histogram bin into a 1D histogram
                    h1D = h2D.ProjectionY(
                        projName, currentRebinnedBin, currentRebinnedBin
                    )

                    # If the current 2D bin is empty skip it
                    if h1D.GetEntries() == 0:
                        # print("empty 1D hist, skipping!")
                        continue

                    # Set the fit limits based on the 1D histogram properties
                    fitMax = h1D.GetMean() + config.nSigmaForFit * h1D.GetRMS()
                    fitMin = h1D.GetMean() - config.nSigmaForFit * h1D.GetRMS()

                    # Obtain fit using JES_BalanceFitter
                    JESBfitter.Fit(h1D, fitMin, fitMax)
                    fit = JESBfitter.GetFit()
                    histFit = JESBfitter.GetHisto()
                    Chi2Ndof = JESBfitter.GetChi2Ndof()

                    # Initialize empty lists to hold the values of each 1D histogram.
                    # There will be as many 1D histograms as Y axis bins of the 3D histogram
                    binEdges = []
                    binEntries = []
                    binErrors = []

                    # Loop over the response bins in the 1D histogram
                    for i in range(
                        1, h1D.GetNbinsX() + 1
                    ):  # Plus one to include last bin
                        binEdges.append(
                            h1D.GetXaxis().GetBinLowEdge(i)
                        )  # Get the current bin edge for plotting later
                        binEntries.append(
                            h1D.GetBinContent(i)
                        )  # Get the number of entries in the bin for plotting later
                        binErrors.append(
                            h1D.GetBinError(i)
                        )  # Get the bin error to get the width of the bin... For plotting later
                    binEdges.append(
                        h1D.GetXaxis().GetBinUpEdge(h1D.GetNbinsX())
                    )  # Append the right most edge of the last bin

                    # Append all the lists for this 1D histogram to the list of lists in the dataframe.
                    # One dataframe per slice, per root file path

                    # Append all the values of the 1D histogram gaussian fit to the lists of the current data frame series
                    # There are ~4 series per data frame, 1 dataframe per slice, per root file path
                    df["x"].loc[TH3Name].append(
                        float(h2D.GetXaxis().GetBinCenter(currentRebinnedBin))
                    )
                    df["y"].loc[TH3Name].append(float(fit.GetParameter(1)))
                    df["xError"].loc[TH3Name].append(
                        float((h2D.GetXaxis().GetBinWidth(currentRebinnedBin) / 2.0))
                    )  # half bin width
                    df["yError"].loc[TH3Name].append(float(fit.GetParError(1)))
                    df["sigma"].loc[TH3Name].append(float(fit.GetParameter(2)))
                    df["sigmaError"].loc[TH3Name].append(float(fit.GetParError(2)))
                    try:
                        df["sigmaOverY"].loc[TH3Name].append(
                            float(fit.GetParameter(2) / float(fit.GetParameter(1)))
                        )
                        df["sigmaOverYError"].loc[TH3Name].append(
                            np.sqrt(
                                (fit.GetParError(2) / fit.GetParameter(2)) ** 2
                                + (fit.GetParError(1) / fit.GetParameter(1)) ** 2
                            )
                        )
                    except:
                        df["sigmaOverY"].loc[TH3Name].append(0)
                        df["sigmaOverYError"].loc[TH3Name].append(0)

                    # Append the lists of 1D histogram info to the list of lists in the dataframe data series
                    df["TH1BinEdges"].loc[TH3Name].append(binEdges)
                    df["TH1BinEntries"].loc[TH3Name].append(binEntries)
                    df["TH1BinErrors"].loc[TH3Name].append(binErrors)

                    df["gaussianParameters"].loc[TH3Name].append(
                        [
                            fit.GetParameter(0),
                            fit.GetParameter(1),
                            fit.GetParameter(2),
                            fitMin,
                            fitMax,
                            Chi2Ndof,
                        ]
                    )
                # break
            df.to_pickle(
                dfPath + ".pickle"
            )  # Save the dataframe to disk as pickle. One dataframe per slice per root file path
        inFile.Close()  # Close the root file
