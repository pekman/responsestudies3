import scipy as scipy
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import mplhep as hep
from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd
import numpy as np
from tqdm import tqdm

COLORS = [
    "tab:blue",
    "tab:orange",
    "tab:green",
    "tab:red",
    "tab:purple",
    "tab:brown",
    "tab:pink",
    "tab:gray",
    "tab:olive",
    "tab:cyan",
]


def plot_energyscales(config):
    # Loop over the file paths
    for rootFilePath in config.listOfRootFilePaths:
        out_path = "/".join(rootFilePath.split("/")[:-1]) + "/output/"
        # Loop over the slices
        for currentSlice in config.slices:
            for xAxisVariable in config.plotDict:
                with PdfPages(
                    config.output_path
                    + "plots/"
                    + xAxisVariable
                    + "_"
                    + "summary"
                    + "_"
                    + "["
                    + str(currentSlice[0])
                    + ","
                    + str(currentSlice[1])
                    + "]"
                    + ".pdf"
                ) as pdf:
                    # Initialize figure 185 mm wide, wiht a 800:600 widht:height ratio
                    f, (ax1) = plt.subplots(
                        1,
                        1,
                        figsize=(18.3 * (1 / 2.54) * 1.7, 13.875 * (1 / 2.54) * 1.32),
                    )

                    # Define a path and name for the dataframe based on the slice range
                    dfPath = (
                        config.output_path
                        + "pickles/"
                        + config.project_name
                        + "_"
                        + config.slicingAxis
                        + "["
                        + str(currentSlice[0])
                        + ","
                        + str(currentSlice[1])
                        + "]"
                    )
                    df = pd.read_pickle(
                        dfPath + ".pickle"
                    )  # Read the appropriate dataframe. One per slice, per file path
                    # campagin = rootFilePath.split("/")[-1].split(".")[0].split("_")[1]
                    campagin = ""

                    # Define markers and colors for plot data series
                    # markers = ["o","^",">","v","<","+","o","^",">","v","<"]
                    [
                        "tab:blue",
                        "tab:orange",
                        "tab:green",
                        "tab:red",
                        "tab:purple",
                        "tab:brown",
                        "tab:pink",
                        "tab:gray",
                        "tab:olive",
                        "tab:cyan",
                    ]
                    j = 0  # Set an iterator for the markes and colors

                    # Iterate over TH3s in data frame, in the reverse order
                    for TH3Name in reversed(df.index):
                        if (
                            TH3Name.split("_-_")[-2] != xAxisVariable
                        ):  # Skip pT plots if we are interested in mjj. For example
                            continue
                        print(
                            "\nPlotting: "
                            + xAxisVariable
                            + str(currentSlice)
                            + " "
                            + TH3Name
                        )

                        # Get data information based on TH3 name
                        numeratorEnergyScale = (
                            TH3Name.split("_-_")[1].split("_")[0].split("-")[0]
                        )
                        denominatorEnergyScale = (
                            TH3Name.split("_-_")[1].split("_")[2].split("-")[1]
                        )

                        # Assign data from dataframe for the current 3D histogram name
                        x = df["x"].loc[TH3Name]
                        y = df["y"].loc[TH3Name]
                        x_error = df["xError"].loc[TH3Name]
                        y_error = df["yError"].loc[TH3Name]

                        ax1.errorbar(
                            x,
                            y,
                            yerr=y_error,
                            xerr=x_error,
                            linestyle="None",
                            marker="o",
                            color=COLORS[j],
                            markersize=2,
                            linewidth=0.5,
                            label=numeratorEnergyScale
                            + "$_{"
                            + xAxisVariable
                            + "}$"
                            + " / "
                            + denominatorEnergyScale.capitalize()
                            + "$_{"
                            + xAxisVariable
                            + "}$",
                        )

                        ax1.axvline(x=400, color="r", label="400 GeV", linewidth=1)
                        ax1.axvline(x=800, color="b", label="800 GeV", linewidth=1)

                        # Add legend
                        leg1 = ax1.legend(
                            borderpad=0.5,
                            loc=1,
                            ncol=2,
                            frameon=True,
                            facecolor="white",
                            framealpha=1,
                        )
                        leg1._legend_box.align = "left"
                        title_og = config.plotDict[xAxisVariable]["legendTitle"]
                        title = (
                            title_og[0]
                            + str(currentSlice[0])
                            + ","
                            + str(currentSlice[1])
                            + title_og[1]
                        )
                        leg1.set_title(title)

                        # Set limits and labels
                        ax1.set_xlim(config.plotDict[xAxisVariable]["xLimits"])
                        ax1.set_ylim(config.plotDict[xAxisVariable]["yLimits"])
                        # ax1.set_xlim(x[start],x[end])
                        # ax1.set_ylim(meann - (rangee / 1.5), meann + (rangee / 1.5))

                        # Set log scale
                        ax1.set_xscale(config.plotDict[xAxisVariable]["xScale"])

                        # Set axis labels
                        ax1.set_xlabel(
                            config.plotDict[xAxisVariable]["xAxisLabel"],
                            fontsize=14,
                            ha="right",
                            x=1.0,
                        )
                        ax1.set_ylabel(
                            config.plotDict[xAxisVariable]["yAxisLabel"],
                            fontsize=14,
                            ha="right",
                            y=1.0,
                        )

                        # Add grid
                        ax1.grid(True)

                        # Add ATLAS label
                        hep.atlas.text("Internal", ax=ax1, loc=0)

                        # Use tight layout
                        plt.tight_layout()

                        pdf.savefig()
                        ax1.clear()
                        # break
                        j += 1  # Increment color and marker iterator
                        # break
                    # break


def plot_individual_fits(config):
    # Loop over the file paths
    for rootFilePath in config.listOfRootFilePaths:
        # Loop over the slices
        for currentSlice in config.slices:
            for xAxisVariable in config.plotDict:
                with PdfPages(
                    config.output_path
                    + "plots/"
                    + xAxisVariable
                    + "_"
                    + "individualFits"
                    + "_"
                    + "["
                    + str(currentSlice[0])
                    + ","
                    + str(currentSlice[1])
                    + "]"
                    + ".pdf"
                ) as pdf:
                    # Initialize figure 185 mm wide, wiht a 800:600 widht:height ratio
                    f, (ax1, ax2) = plt.subplots(
                        1,
                        2,
                        figsize=(18.3 * (1 / 2.54) * 1.7, 13.875 * (1 / 2.54) * 1.32),
                    )

                    # Define a path and name for the dataframe based on the slice range
                    dfPath = (
                        config.output_path
                        + "pickles/"
                        + config.project_name
                        + "_"
                        + config.slicingAxis
                        + "["
                        + str(currentSlice[0])
                        + ","
                        + str(currentSlice[1])
                        + "]"
                    )
                    df = pd.read_pickle(
                        dfPath + ".pickle"
                    )  # Read the appropriate dataframe. One per slice, per file path
                    # campagin = rootFilePath.split("/")[-1].split(".")[0].split("_")[1]
                    campagin = ""

                    # Define markers and colors for plot data series
                    # markers = ["o","^",">","v","<","+","o","^",">","v","<"]

                    j = 0  # Set an iterator for the markes and colors

                    # Iterate over TH3s in data frame, in the reverse order
                    for TH3Name in reversed(df.index):
                        if (
                            TH3Name.split("_-_")[-2] != xAxisVariable
                        ):  # Skip pT plots if we are interested in mjj. For example
                            continue
                        print(
                            "\nPlotting: "
                            + xAxisVariable
                            + str(currentSlice)
                            + " "
                            + TH3Name
                        )

                        # Get data information based on TH3 name
                        numeratorEnergyScale = (
                            TH3Name.split("_-_")[1].split("_")[0].split("-")[0]
                        )
                        denominatorEnergyScale = (
                            TH3Name.split("_-_")[1].split("_")[2].split("-")[1]
                        )

                        # Assign data from dataframe for the current 3D histogram name
                        x = df["x"].loc[TH3Name]
                        y = df["y"].loc[TH3Name]
                        x_error = df["xError"].loc[TH3Name]
                        y_error = df["yError"].loc[TH3Name]

                        print("bin number out of " + str(len(x)) + ": ", end="")
                        # test_i = 0
                        for i in tqdm(range(0, len(df["x"].loc[TH3Name]))):
                            # for i in range(0, 200):
                            # test_i += 1
                            # if test_i > 10: break
                            # print(str(i) + ", ", end="")
                            # Plot data
                            ax1.errorbar(
                                x,
                                y,
                                yerr=y_error,
                                xerr=x_error,
                                linestyle="None",
                                marker="o",
                                color=COLORS[j],
                                markersize=2,
                                linewidth=0.5,
                                label=numeratorEnergyScale
                                + "$_{"
                                + xAxisVariable
                                + "}$"
                                + " / "
                                + denominatorEnergyScale.capitalize()
                                + "$_{"
                                + xAxisVariable
                                + "}$",
                            )

                            ax1.errorbar(
                                x[i],
                                y[i],
                                yerr=y_error[i],
                                xerr=x_error[i],
                                linestyle="None",
                                marker="o",
                                color="lime",
                                markersize=2,
                                linewidth=0.5,
                                label=config.plotDict[xAxisVariable]["yAxisLabel"]
                                + " at "
                                + str(x[i]),
                            )

                            ax1.axvline(x=400, color="r", label="400 GeV", linewidth=1)
                            ax1.axvline(x=800, color="b", label="800 GeV", linewidth=1)

                            # Add legend
                            leg1 = ax1.legend(
                                borderpad=0.5,
                                loc=1,
                                ncol=2,
                                frameon=True,
                                facecolor="white",
                                framealpha=1,
                            )
                            leg1._legend_box.align = "left"
                            title_og = config.plotDict[xAxisVariable]["legendTitle"]
                            title = (
                                title_og[0]
                                + str(currentSlice[0])
                                + ","
                                + str(currentSlice[1])
                                + title_og[1]
                            )
                            leg1.set_title(title)

                            # Set limits and labels
                            ax1.set_xlim(config.plotDict[xAxisVariable]["xLimits"])
                            ax1.set_ylim(config.plotDict[xAxisVariable]["yLimits"])

                            # Set log scale
                            ax1.set_xscale(config.plotDict[xAxisVariable]["xScale"])

                            # Set axis labels
                            ax1.set_xlabel(
                                config.plotDict[xAxisVariable]["xAxisLabel"],
                                fontsize=14,
                                ha="right",
                                x=1.0,
                            )
                            ax1.set_ylabel(
                                config.plotDict[xAxisVariable]["yAxisLabel"],
                                fontsize=14,
                                ha="right",
                                y=1.0,
                            )

                            # Add grid
                            ax1.grid(True)

                            # Add ATLAS label
                            # hep.atlas.text("Internal",ax=ax1)

                            ## Right subplot
                            hep.histplot(
                                df["TH1BinEntries"].loc[TH3Name][i],
                                df["TH1BinEdges"].loc[TH3Name][i],
                                yerr=df["TH1BinErrors"].loc[TH3Name][i],
                                edges=True,
                                label=config.plotDict[xAxisVariable]["yAxisLabel"]
                                + " at "
                                + str(x[i]),
                            )
                            p0 = df["gaussianParameters"].loc[TH3Name][i][0]
                            p1 = df["gaussianParameters"].loc[TH3Name][i][1]
                            p2 = df["gaussianParameters"].loc[TH3Name][i][2]
                            fitMin = df["gaussianParameters"].loc[TH3Name][i][3]
                            fitMax = df["gaussianParameters"].loc[TH3Name][i][4]
                            Chi2Ndof = df["gaussianParameters"].loc[TH3Name][i][5]

                            x_fit = np.linspace(fitMin, fitMax, 100)
                            y_fit = p0 * np.exp(-0.5 * ((x_fit - p1) / p2) ** 2)

                            ax2.plot(x_fit, y_fit, label="Fit", color="red")

                            # Set limits and labels 1
                            ax2.set_xlim(0.5, 1.5)
                            ax2.set_xlabel(
                                config.plotDict[xAxisVariable]["yAxisLabel"],
                                ha="right",
                                x=1.0,
                            )
                            ax2.set_ylabel("Counts", ha="right", y=1.0)
                            ax2.ticklabel_format(
                                style="sci", axis="y", scilimits=(0.001, 1e10)
                            )

                            # Legend 2
                            leg2 = ax2.legend(borderpad=0.5, frameon=False, loc=2)
                            # print(fileName)
                            # numerator = fileName.split("_")[2].split(".")[0]
                            leg2._legend_box.align = "left"  # Align legend title
                            leg2.set_title("Chi2Ndof = " + str(round(Chi2Ndof, 2)))
                            # hep.atlas.text("Internal",ax=ax2)

                            # Use tight layout
                            plt.tight_layout()

                            pdf.savefig()
                            ax1.clear()
                            ax2.clear()
                            # break
                        j += 1  # Increment color and marker iterator
                        # break
                    # break
