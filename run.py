import numpy
import pandas as pd
import ROOT
import sys
import argparse
import importlib
import modules.plotting as plotting
import modules.processing as processing


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--project", help="Project name")
    parser.add_argument("-m", "--mode", help="Which mode to run")

    args = parser.parse_args()
    return args


def get_config(project_name):
    module_string = f"projects.{project_name}.{project_name}_config"
    config_module = importlib.import_module(module_string)
    conf = config_module.Config(project_name)
    return conf


def main():
    args = get_arguments()
    config = get_config(args.project)

    if args.mode == "process":
        processing.process(config)
    elif args.mode == "plot1":
        plotting.plot_energyscales(config)
    elif args.mode == "plot2":
        plotting.plot_individual_fits(config)


if __name__ == "__main__":
    main()
